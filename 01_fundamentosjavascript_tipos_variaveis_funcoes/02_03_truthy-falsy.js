const usuarioLogado = true;
const contaPaga = false;

//Truthy ou falsy
//variáveis que não são nem true nem false, mas, que para o JavaScript, eles equivalem a ser tipo verdadeiro ou tipo falso, ou como costumamos dizer, truthy ou falsy.

//0 => false
//1 => true

console.log('0 == false => true ',0==false);
console.log('0 == true => false ',0==true);
console.log('-------------------');
console.log('"" == false => true ',""==false);
console.log('" " == true => false '," "== true);
console.log('-------------------');
console.log('1 == true => true ',1==true);
console.log('-------------------');
console.log('null undefined');//representa o vazio...nao tem nada
let minhaVariavel;//-> aqui o js coloca como undefined, nada foi denifinido 
console.log(minhaVariavel);
let varNull = null;//-> declarei como o valor nulo
console.log(varNull);
//o null e o undefined são similares e podem ser usados, entre eles, sem problema nenhum.

console.log('-------------------');
let numero = 3;
let texto = "Alura";
console.log(typeof numero);
console.log(typeof texto);
console.log('-------------------');
console.log(typeof minhaVariavel);
console.log(typeof varNull);//retorna como objeto....é um bug