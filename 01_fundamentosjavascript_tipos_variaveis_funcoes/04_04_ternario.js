const idadeMinima = 18;
const idadeCliente = 19;

console.log('IF normal: ');
if (idadeCliente >= idadeMinima){
    console.log('Cerveja');
}else{
    console.log('Suco');
}
console.log('IF ternario: ');
console.log(idadeCliente >=idadeMinima ?'Cerveja':'Suco');