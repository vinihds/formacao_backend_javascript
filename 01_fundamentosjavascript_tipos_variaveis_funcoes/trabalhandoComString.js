function testeUm() {
    const cidade = "belo horizonte";
    const input = "Belo Horizonte";

    console.log(cidade === input); // false
}

function testeDois() {
    const cidade = "belo horizonte";
    const input = "Belo Horizonte";

    console.log(cidade == input); // false
}

function testeTres() {
    const cidade = "belo horizonte";
    const input = "Belo Horizonte";

    const inputMinusculo = input.toLowerCase();

    console.log(cidade === inputMinusculo); // true
}

function testeQuatro() {
    const cidade = "BELO HORIZONTE";
    const input = "Belo Horizonte";

    const inputMaiusculo = input.toUpperCase();

    console.log(cidade === inputMaiusculo); // true
}

function testeCinco() {
    const senha = "minhaSenha123"
    console.log('funcao length da frase "minhaSenha123" é ',senha.length) // 13 caracteres
}

// testeUm();
// testeDois();
// testeTres();
// testeQuatro();
testeCinco();