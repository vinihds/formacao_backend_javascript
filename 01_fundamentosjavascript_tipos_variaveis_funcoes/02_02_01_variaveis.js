var altura = 5;
var comprimento = 7;

area = altura * comprimento;
//nao preciso declarar ...mas é uma boa pratica utilizar a declaracao
//porem eu posso declarar depois....o node le todo o arquivo e mapea as variaveis
console.log(area);
var area; //posso declarar depois

console.log('----------------------------');
let forma1 = 'retangulo';
let altura1 = 5;
let comprimento1 = 7;
let area1;

if (forma1 === 'retangulo'){
    area1 = altura1 * comprimento1;
}else{
    area1 = (altura1 * comprimento1)/2;
}

console.log(area1);