var lista = [10,1, 5, 9, 8, 12, 15];
var retorno = lista.sort();
console.log(retorno);
console.log('----------------------');
retorno = lista.sort(comparaNumeros);
console.log(retorno);
console.log('----------------------');
retorno = lista.sort(comparaNumeros);
console.log(retorno);
console.log('----------------------');
retorno = lista.sort((a, b) => a - b);
console.log(retorno);
console.log('----------------------');
lista = ['10','1', '5', '9', '8', '12', '15'];
retorno = lista.sort();
console.log(retorno);
console.log('----------------------');
lista = ['a','aa', 'b', 'ab', 'ba', 'bb', 'c'];
retorno = lista.sort();
console.log(retorno);
console.log('----------------------');

function comparaNumeros(a,b) {
    if (a == b) return 0; 
    if (a < b) return -1; 
    if (a > b) return 1; 
}
