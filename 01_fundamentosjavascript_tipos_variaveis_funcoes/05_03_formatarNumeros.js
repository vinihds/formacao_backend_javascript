const sal = 3000;
const semana = 173;

function ganhoPorHora(salario, horasTrabalhadas) {
    const salarioHora = salario/horasTrabalhadas;
    return salarioHora;    
}

console.log(ganhoPorHora(sal,semana));

function ganhoPorHora1(salario, horasTrabalhadas) {
    const salarioHora = salario/horasTrabalhadas;
    // Math.round() retorna o valor de um número arredondado para o inteiro mais próximo.
    return Math.round(salarioHora);    
}

console.log('Math.round: ',ganhoPorHora1(sal,semana));
// resultado é 17.045454545454547

function ganhoPorHora2(salario, horasTrabalhadas) {
    const salarioHora = salario/horasTrabalhadas;
    // arredonda o número para cima
    return salarioHora.toFixed(2);    
}

console.log('toFixed(2): ',ganhoPorHora2(sal,semana));
//arradonda para cima...17.045454545454547 => 17,05

function ganhoPorHora3(salario, horasTrabalhadas) {
    const salarioHora = salario/horasTrabalhadas;
    // arredonda o número para cima
    return salarioHora.toLocaleString('pt-BR',{style:'currency',currency: 'BRL'}) 
}

console.log("toLocaleString('pt-BR',{style:'currency',currency: 'BRL'}) ",ganhoPorHora3(sal,semana));


console.log('Math.ceil: ',Math.ceil(11.123));
// retorna o maior número inteiro que é maior

console.log('Math.floor: ', Math.floor(11.789));
// retorna o menor número inteiro que é menor que o número passado