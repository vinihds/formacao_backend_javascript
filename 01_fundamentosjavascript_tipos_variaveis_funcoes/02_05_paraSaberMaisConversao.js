let telefone = 12341234;
console.log("O telefone é " + String(telefone)); 
console.log("O telefone é " + telefone); 
// teremos a conversão do número 12341234 para uma string “12341234” e assim poderemos fazer a concatenação entre as strings
console.log('----------------');
let usuarioConectado = false;
console.log("teste "+String(usuarioConectado));
console.log("teste "+usuarioConectado);
console.log('----------------');
let largura = "10";
let altura = "5";
console.log(Number(largura)*Number(altura));
console.log(+largura*+altura);

console.log('----------------');
usuarioConectado= false;
console.log (Number( usuarioConectado ) ); // teremos a conversão da booleana para número, sendo que false (falso) retorna o número 0.
usuarioConectado= true;
console.log (Number( usuarioConectado ) ); // agora teremos a conversão de true (verdadeiro) para o número 1.