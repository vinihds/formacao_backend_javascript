//tipo de dado

//boolean

//conversao implicita
const numero = 456;
const numeroString = "456";

console.log(numero === numeroString);
console.log(numero == numeroString);

console.log('vai concatenar o numero com a string ',numero + numeroString);
//conversao explicita
//Numero() -> converte string para numero
//String() -> Converte numero para string

console.log('converte para numero ',numero + Number(numeroString));


