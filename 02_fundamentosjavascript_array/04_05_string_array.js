const nome = 'Alura';
let nomeMaiuscula = '';
//NAO DA PARA PERCORRER USANDO FOREACH
// nome.forEach(elemento => {
//     console.log(elemento);
// });

for (let index = 0; index < nome.length; index++) {
    nomeMaiuscula += nome[index].toUpperCase();
}

console.log(nomeMaiuscula);

console.log('==========================');
console.log('Concat');
const nomeDoCurso = 'Fundamentos de JS';
const nomeDaPlataforma = 'Alura';