const listaNomes = ['Ana', 'Ju', 'Leo', 'Paula'];

function imprimeNome(nome){
    console.log(nome);
}

listaNomes.forEach(imprimeNome);
console.log('==========================');
listaNomes.forEach(function (nome){
    console.log(nome);
});
console.log('==========================');
listaNomes.forEach(nome => {
    console.log(nome);
});
