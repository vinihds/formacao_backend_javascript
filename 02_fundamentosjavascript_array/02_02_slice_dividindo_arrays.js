const nomes = ['João', 'Juliana', 'Ana' , 'Caio','Lara','Marjore','Guilherme', 'Aline','Fabiana', 'Andre', 'Carlos', 'Paulo', 'Bia', 'Vivian', 'Isabela', 'Vinicius', 'Renan', 'Renata', 'Daisy'];
console.log(`Inscritos ${nomes}`);
//nomes.slice(INICIO,FIM);
const sala1 = nomes.slice(0,nomes.length/2);
console.log(`Sala 1 ${sala1}`);
//nomes.splice(FIM); --> AUTOMATICAMENTE O INICIO É ZERO
const sala2 = nomes.splice(nomes.length/2);
console.log(`Sala 2 ${sala2}`);
let meuArray = [1,2,3,4,5,6]
console.log(meuArray.splice(2));