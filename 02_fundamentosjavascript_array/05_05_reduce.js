const salaJS = [7,8,8,7,10,6.5,4,10,7];
const salaJava = [6,5,8,9,5,6];
const salaPython = [7,3.5,8,9.5]; 

function mediaSala(notasDaSala) {
    const somaDasNotas = notasDaSala.reduce((acumulador,atual) => {
        return acumulador += atual;
    },0);
    return somaDasNotas/notasDaSala.length;
}

console.log(`A média da sala de JS é ${mediaSala(salaJS)}`);
console.log(`A média da sala de JAVA é ${mediaSala(salaJava)}`);
console.log(`A média da sala de Pyton é ${mediaSala(salaPython)}`);

const notas = [10, 6.6,8,7];
const media = notas.reduce((acumulador,item) => {
    return acumulador+item;
},0)/notas.length;
console.log(`media ${media}`);