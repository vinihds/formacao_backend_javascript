const alunos = ['João', 'Juliana','Ana', 'Caio','Ana'];
const mediaDosAlunos = [10,7,1,9,6];

let listaDeNotasEAlunos = [alunos,mediaDosAlunos];

const exibeNomeNota = (nomeAluno) => {
    //include verifica  se existe dentro do array
    if (listaDeNotasEAlunos[0].includes(nomeAluno)){
        //indexOf retorna o conteudo do primeiro conteudo referente a esse indice
        let indice = listaDeNotasEAlunos[0].indexOf(nomeAluno);
        return `${listaDeNotasEAlunos[0][indice]} sua média é ${listaDeNotasEAlunos[1][indice]}`;
    }else{
        return `Aluno não Cadastrado`;
    }
}

console.log(exibeNomeNota('Vinicius'));
console.log(exibeNomeNota('Ana'));
console.log(exibeNomeNota("Juliana"));