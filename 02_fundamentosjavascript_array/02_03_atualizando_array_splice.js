// O METODO SPLICE ALTERA O ARRAY QUE INVOCOU

let listaDeChamada = ['João', 'Ana', 'Caio','Lara','Marjore','Leo'];
//ANA E O CAIO MUDARAM DE ESCOLA
//RODRIGOU ENTROU NA SALA
//UTILIZAR O SPLICE PARA REMOVER E INCLUIR
//splice(inicio,quantidade de elementos que quero remover,'Rodrigo') 
console.log(`Lista de Chamada ORIGINAL ${listaDeChamada}`);
listaDeChamada.splice(1,2,'Rodrigo');

console.log(`Lista de Chamada ${listaDeChamada}`);

listaDeChamada = ['João', 'Ana', 'Caio','Lara','Marjore','Leo'];
console.log('================================');
console.log(`Lista de Chamada ORIGINAL ${listaDeChamada}`);
listaDeChamada.splice(1,2);
console.log(`Lista de Chamada ${listaDeChamada}`);
console.log('================================');
console.log('INCLUIR ELEMENTOS SEM REMOVER');
//                0      1      2
listaDeChamada = ['João', 'Ana', 'Caio',];
console.log(`Lista de Chamada ORIGINAL ${listaDeChamada}`);
//splice(INDICE,QUANTIDADE DE ELEMENTOS QUE QUERO REMOVER SE COLOCAR ZERO NAO REMOVO NINGUEM,'RODRIGO');
listaDeChamada.splice(2,0,'RODRIGO');//NESSE CASO O ARRAY VAI FICAR [JOAO, ANA, RODIRGO, CAIO]
console.log(`Lista de Chamada ${listaDeChamada}`);

console.log('================================');
//                0      1      2
listaDeChamada = ['João', 'Ana', 'Caio',];
console.log(`Lista de Chamada ORIGINAL ${listaDeChamada}`);
//splice(INDICE,QUANTIDADE DE ELEMENTOS QUE QUERO REMOVER SE COLOCAR ZERO NAO REMOVO NINGUEM,'RODRIGO');
listaDeChamada.splice(4,0,'RODRIGO');//NESSE CASO O ARRAY VAI FICAR [JOAO, ANA, RODIRGO, CAIO]
console.log(`Lista de Chamada ${listaDeChamada}`);


let meuArray = ['1','2','3','4','5','6']
console.log(meuArray);
meuArray.splice(7,0,'D')
console.log(meuArray);
