const notas = [10, 6.5, 8 , 7.5];

let somaDasNotas = 0;
notas.forEach(function(nota){
    console.log(nota);
});
console.log('===========================');
notas.forEach(elemento => {
    somaDasNotas+=elemento;
});
console.log(somaDasNotas/notas.length);
console.log('===========================');
let somatorioMedia = 0;
notas.forEach(function(nota){
    somatorioMedia +=nota;
})
console.log(`Media somatorio ${somatorioMedia/notas.length}`);
console.log('===========================');