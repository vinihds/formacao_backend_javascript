var arrayNumerico = [0,1,2,3,4,5,6,7,8,9,10];

const incluirPrimeiraPosicao = (valor) =>{
        arrayNumerico.unshift(valor);
    }
const incluirUltimaPosicao = (valor) => {
    arrayNumerico.push(valor)
}

const removePrimeiraPosicao = () =>{
    arrayNumerico.shift();
}

const removeUltimaPosicao = () => {
    arrayNumerico.pop();
}

console.log(`Array Original ${arrayNumerico}`);

incluirPrimeiraPosicao(-1);
console.log(`Array Incluir Primeira Posicao Unshift ${arrayNumerico}`);

incluirUltimaPosicao(11);
console.log(`Array Incluir Ultima Posicao push ${arrayNumerico}`);

removePrimeiraPosicao();
console.log(`Array Remove Primeira Posicao shift ${arrayNumerico}`);

removeUltimaPosicao();
console.log(`Array Remove Ultima Posicao pop ${arrayNumerico}`);