/*
Map => para cada item e retorna o valor do item equivalente no array resultante
O método map() não modifica o array original, ele retorna um novo array com os itens resultantes do mapeamento.
*/

/*
Exemplo: Elevar ao quadrado o conteudo de um array
*/

/*Tradicional*/

const funcaoTradicional = () => {
    let arrayNumerico = [1,2,3,4,5,6,7,8,9,10];
    let arrayRetorno = [];
    arrayNumerico.forEach( (elemento,contador) => {
        arrayRetorno[contador] = elemento * 2;
    });
    return arrayRetorno;
}

console.log(`Array Tradiconal Duplicado ${funcaoTradicional()}`);

/*
Sintaxa MAP

arrayOriginal.map(callback)

callback é uma função que será executada para cada elemento no vetor original e retornará uma representação 
dele com base em alguma lógica, que será o item equivalente no vetor resultante

Estrutura do callback
function(valorAtual, indice, array)

valorAtual é obrigatório e representa o próprio item da iteração atual
indice é opcional e representa o índice do item da iteração atual
array também é opcional e representa o próprio array ao qual os itens pertencem

retornado um novo array
*/

const funcaoMapSimples = () => {
    let arrayNumerico = [1,2,3,4,5,6,7,8,9,10];
    let arrayRetorno = [];
    arrayRetorno = arrayNumerico.map(function (valor){
        return valor * 2;
    });
    return arrayRetorno;
}

console.log(`Array Map Duplicado ${funcaoMapSimples()}`);

const funcaoMapSimplesComIndice = () => {
    let arrayNumerico = [1,2,3,4,5,6,7,8,9,10];
    let arrayRetorno = [];
    arrayRetorno = arrayNumerico.map(function (valor,indice){
        console.log(`Indice ${indice}`);
        return valor * 2;
    });
    return arrayRetorno;
}

console.log(`Array Map Duplicado Com indice ${funcaoMapSimplesComIndice()}`);

const funcaoMapArrowFunction = () => {
    let arrayNumerico = [1,2,3,4,5,6,7,8,9,10];
    let arrayRetorno = [];
    arrayRetorno = arrayNumerico.map(valor => {
        return valor * 3;
    });
    return arrayRetorno
}

console.log(`Array Map Duplicado Com Arrow Function ${funcaoMapArrowFunction()}`);