const notas = [10,9,8,7,6];
const notasAtualizadas2 = notas.map(nota => nota+1);
console.log(notasAtualizadas2);
console.log('==============');
const notasAtualizadas3 = notas.map(nota => nota==10?nota:nota+1);
console.log(notasAtualizadas3)
console.log('==============');
const notasAtualizadas4 = notas.map(nota => nota==10?nota:++nota);
console.log(notasAtualizadas4)
console.log('==============');
const notasAtualizadas = notas.map( elemento => {
    console.log(elemento);
    if (elemento<=9)
        return elemento+1
    else   
        return elemento;
});
console.log(notasAtualizadas)