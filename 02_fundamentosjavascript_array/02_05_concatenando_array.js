//JUNSTANDO ARRAY
const salaDePython = ['Melissa','Helena','Rodrigo'];
const salaDeJavaScript = ['Ju','Leo','Raquel'];

const salasUnificadas = salaDePython.concat(salaDeJavaScript);
console.log(`Sala de Python ${salaDePython}`);
console.log(`Sala de JavaScript ${salaDeJavaScript}`);
console.log(`Salas unificadas ${salasUnificadas}`);
console.log('-------------------');
console.log([...salaDePython,...salaDeJavaScript]);
console.log(salasUnificadas);
//o metodo concat ele me retorna um array concatenado