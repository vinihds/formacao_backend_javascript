// o reduce busca reduzir um array. 
// Ele iterará por cada elemento dessa lista com o objetivo de ao final gerar um único valor (de qualquer tipo), como por exemplo a soma de todos os elementos desse array. 
// Lembrando que não ficamos presos apenas a números.

const funcaoTradicional = () => {
    console.log('Somatorio Tradicional');
    let total = 0;
    let numeros = [1, 2, 3, 4, 5, 6];
    for ( let i = 0; i < numeros.length; i++ ){
        total += numeros[i];
    }
    console.log(total);
    console.log('======================');
}

/*
    Assinatura da função

    array.reduce(callback[, initialValue])

    array: Vetor de origem sobre o qual estamos iterando para aplicar a função;
    callback: Função a ser chamada para cada item do array de origem e cujo o retorno produz um valor final baseado em alguma regra
    initialValue: É o valor inicial do resultado da operação que será passado para a função de callback na primeira iteração

*/

const funcaoUsandoReduce = () => {
    let total = 0;
    let numeros = [1, 2, 3, 4, 5, 6];
    total = numeros.reduce(function(total,numero){
        return total + numero;
    },0);
    return total;
}

const funcaoUsandoReduceArrowFunction = () => {
    let total = 0;
    let numeros = [1, 2, 3, 4, 5, 6];
    total = numeros.reduce((total,numero) => {
        return total + numero;
    },0);
    return total;
}

const reduceComplexo = () => {
    console.log('reduce');
    var numeros = [0, 10, 20, 30, 40, 50, 60];
    var total = numeros.reduce((acumulador, numero, indice, original) => {
    console.info(`${acumulador} total até o momento`);
    console.log(`${numero} valor atual`);
    console.log(`${indice} indice atual`);
    console.log(`${original} array original`);
    return acumulador += numero;
    }, 20)
    console.warn('acaboooou');
    console.log(total)
}
const funcao = () =>{
    console.log('======================');
    console.log('======================');
}

//funcaoTradicional();
console.log(`Somatorio usando reduce: ${funcaoUsandoReduce()}`);
console.log(`Somatorio usando reduce arrow function: ${funcaoUsandoReduceArrowFunction()}`);

