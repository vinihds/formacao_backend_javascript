const alunos = ['João', 'Juliana', 'Caio','Ana'];
const mediaDosAlunos = [10,7,9,6];

let listaDeNotasEAlunos = [alunos,mediaDosAlunos];
console.log(`Imprimindo tudo ${listaDeNotasEAlunos}`);//[ [ 'João', 'Juliana', 'Caio', 'Ana' ], [ 10, 7, 9, 6 ] ]
console.log(`Imprimindo a lista de alunos ${listaDeNotasEAlunos[0]}`)//[ 'João', 'Juliana', 'Caio', 'Ana' ]
console.log(`Imprimindo a lista com a media dos alunos ${listaDeNotasEAlunos[1]}`)//[ 10, 7, 9, 6 ]
console.log(`Imprimindo o primeiro aluno ${listaDeNotasEAlunos[0][0]} e sua é média é ${listaDeNotasEAlunos[1][0]}`);
console.log(`Imprimindo o segundo aluno ${listaDeNotasEAlunos[0][1]} e sua média é ${listaDeNotasEAlunos[1][1]}`);

listaDeNotasEAlunos = alunos.concat(mediaDosAlunos);
console.log(`Teste usando o concat${listaDeNotasEAlunos}`);