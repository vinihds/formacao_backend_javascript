// no JavaScript, para trabalhar com objetos, temos que ter a ideia, a noção, que ele é formado, todo objeto no JavaScript ele é formado com a chave valor, diferente de um array, que o array você tem lá os elementos do array e você consegue acessar pelo índice.

const listaCPFs = ["123123123","87984565623","1518423162543"]
console.log(listaCPFs);

const cliente = ["nome","Vini","idade",36];
console.log(cliente);
console.log(`Nome do cliente é ${cliente[1]} e sua idade é ${cliente[3]}`);
const clienteObjeto = {
    nome: "Vinicius",
    idade: "36",
    cpf: "112341342134"
}
console.log(`Nome do cliente é ${clienteObjeto.nome} e sua idade é ${clienteObjeto.idade}`);