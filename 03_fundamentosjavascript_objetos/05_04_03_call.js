/*
call()
Esse método permite que uma função seja chamada com parâmetros e valor de this específicos.
*/

function imprimeNomeEmail(tipoCliente){
    console.log(`${tipoCliente} - nome ${this.nome}, email ${this.email}`);
}

const cliente1 = {
    nome: "Carlos",
    email: "c@email.com"
}
   
const cliente2 = {
    nome: "Fred",
    email: "f@email.com"
}

imprimeNomeEmail.call(cliente1, "cliente especial");
imprimeNomeEmail.call(cliente2, "cliente estudante")

/*
Esta função esta sendo chamada como objeto do metodo call, podemos especificar que o contexto this em cada chama se refere a um objeto diferente
(cliente1, cliente2) sem a necessidade de assinar a função de cada objeto

*/