/*
Quando um objeto javascript é criado ele tem as propriedades particulares (nome,cpf,....) e outras herdadas do prototipo
Alem do nome e valor de cada propriedade, cadas propriedade tem 3 atributos.
 1 - writable: define se as propriedades  podem ser adicionadas (ou escritas em um objeto)
 2 - enumerable: define se a propriedade é retornada (exemplo) em um llog for...in ou utilizando Object.keys / Object.values / Object.entries. Ou seja, se a propriedade é enumeravel.
 3 - Configurable: especifica se a propriedade pode ser modifica ou deletada. Ou seja (configuravel)

 As propriedades criadas durante o desenvolvimento são true. 
 Já a maior parte das propriedades herdadas tem atributos como false. 
*/

const catalogo = {
    "editora": "Casa do Código",
    "catalogo": [
    {
      "id": 50,
      "titulo": "Primeiros Passos com NodeJS",
      "autor": "João Rubens",
      "categoria": "programação",
      "versoes": ["ebook", "impresso"]
    }
   ]}

//O metodo Object,getOwnPropertyDescriptor descreve os atributos writable / enumerable / configurable

console.log(Object.getOwnPropertyDescriptor(catalogo, "editora"))
/*
{
  value: 'Casa do Código',
  writable: true,
  enumerable: true,       
  configurable: true      
}

As propriedades da editora foram criadas pelo desenvolvedor e nao herdadas
*/
console.log('=========================');
function Cliente(nome,cpf,email,saldo){
    this.nome = nome;
    this.cpf = cpf;
    this.email = email;
    this.saldo = saldo;
    this.depositar = function(valor){
        this.saldo += valor;
    }
}
console.log(Object.getOwnPropertyDescriptor(Cliente,nome));