class Pessoa{
    constructor(nome,sobrenome,cpf,email){
        this.nome = nome;
        this.sobrenome = sobrenome;
        this.cpf = cpf;
        this.email = email;
    }
}

class Programador extends Pessoa{
    constructor(nome,sobrenome,cpf,email,linguagem){
        super(nome,sobrenome,cpf,email);
        this.linguagem = linguagem;
    }
}

const novoDev = new Programador("Vinicius","Souza",123123,"vini@teste.com","Java");
console.log(novoDev);