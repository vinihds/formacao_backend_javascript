function Cliente(nome,cpf,email,saldo){
    this.nome = nome;
    this.cpf = cpf;
    this.email = email;
    this.saldo = saldo;
    this.depositar = function(valor){
        this.saldo += valor;
    }
}

const andre = new Cliente("Vinicius","123123","vinicius@teste.com",0);
for (registro in andre){
    console.table(`${registro} - ${andre[registro]}`);
}
console.log('=============');
console.log(andre);