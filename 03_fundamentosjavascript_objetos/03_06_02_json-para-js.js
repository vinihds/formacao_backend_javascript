//JSON.stringify(): converte um objeto javascript para json;

const objetoJS = {
    id: 50,
    titulo: "Titulo do livro",
    autor:"Joao",
    categoria:"programacao",
    versoes : ["ebook","impresso"]
};
console.log('Objeto java script');
console.log(objetoJS);
const jsonLivro = JSON.stringify(objetoJS);
console.log('objeto json');
console.log(jsonLivro);
console.log('voltando para JS');
const objetoJSVolta = JSON.parse(jsonLivro);
console.log(objetoJSVolta);