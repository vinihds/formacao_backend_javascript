//Extria de uma listagem de Clientes apenas as informações dos dependentes

//juntar dados num lugar só

const clientes = [
{
    nome: "Vinicius",
    idade: "36",
    cpf: "112341342134",
    email: "teste@teste.com",
    telefone : ["1122","3344"],
    dependentes:  [{
        nome: "Sarah",
        parentesco: "filho",
        dataNasc: "20/03/2021"
    },{
        nome: "Ivete",
        parentesco: "filho mais velho",
        dataNasc: "20/03/1990"
    }],
    saldo:100
},
{
    nome: "Juliana",
    idade: "25",
    cpf: "112341342134",
    dependentes:  [{
        nome: "Sarah",
        parentesco: "filho",
        dataNasc: "20/03/2021"
    },{
        nome: "Ivete",
        parentesco: "filho mais velho",
        dataNasc: "20/03/1990"
    }],
    saldo:100
}]
//console.table(clientes)
// spread-operator operador de espalhamento
//espalhamos os arrays dentro de um unico array
const listaDepentes = [...clientes[0].dependentes,...clientes[1].dependentes]
console.log(listaDepentes);

// desafio ->  invés de ir listando no índice zero, no índice um, pode fazer um loop no array clientes e para cada um deles ver como é que você puxa o espalhamento de cada um dos dependentes
console.error('===============================');
let desafio = [];
clientes.forEach(elemento => {
    // console.table(elemento.dependentes);
    desafio.push(...elemento.dependentes)
    // console.table(elemento.dependentes);
});
console.table(desafio);