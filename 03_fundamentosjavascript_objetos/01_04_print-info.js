const cliente = ["nome","Vini","idade",36];
console.log(cliente);
console.log(`Nome do cliente é ${cliente[1]} e sua idade é ${cliente[3]}`);
const clienteObjeto = {
    nome: "Vinicius",
    idade: "36",
    cpf: "112341342134"
}
console.log(`Nome do cliente é ${clienteObjeto.nome} e sua idade é ${clienteObjeto.idade}`);
console.log(clienteObjeto.nome.toUpperCase());
console.log(clienteObjeto.cpf.substring(3,6));