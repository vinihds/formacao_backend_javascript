{
    console.log('Objeto literal');
    const objPersonagem = {
        nome: "Gandalf",
        classe: "mago",
        nivel: "20"
    }

    //    Um objeto literal é um objeto criado com a notação literal, ou seja: uma lista de chave e valores dentro de chaves { }, que atribuímos a uma variável para que o valor possa ser acessado depois

    // objeto literal sempre aponta para um mesmo local na memória, mesmo se você criar cópias dele

    console.log(`objPersonagem.nome: ${objPersonagem.nome}`)
    const objPersonagem2 = objPersonagem
    objPersonagem2.nome = "Gandalf, o Cinzento"

    console.log(`objPersonagem.nome: ${objPersonagem.nome}`) //Gandalf, o Cinzento
    console.log(`objPersonagem2.nome: ${objPersonagem2.nome}`) //Gandalf, o Cinzento
}
// A variável objPersonagem2 não fez uma cópia do objeto original, apenas serviu como referência para o objeto original objPersonagem. Assim, qualquer alteração em qualquer um dos objetos altera ambos

// JavaScript, quando trabalha com objetos, acessa os valores deles fazendo referência ao original. mas não cria uma cópia

// Como podemos contornar utilizando Object.create()
{
    const objPersonagem = {
        nome: "Gandalf",
        classe: "mago",
        nivel: "20"
       }
       
       const objPersonagem2 = Object.create(objPersonagem)
       objPersonagem2.nome = "Gandalf, o Cinzento"
       
       console.log(objPersonagem.nome) //Gandalf
       console.log(objPersonagem2.nome) //Gandalf, o Cinzento
}

// O método Object.create() cria um novo objeto utilizando como protótipo o objeto passado via parâmetro