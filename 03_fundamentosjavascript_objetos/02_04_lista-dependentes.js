let clienteObjeto = {
    nome: "Vinicius",
    idade: "36",
    cpf: "112341342134",
    email: "teste@teste.com",
    telefone : ["1122","3344"],
    dependente:  {
        nome: "Sarah",
        parentesco: "filho",
        dataNasc: "20/03/2021"
    }
}

console.log(clienteObjeto);
console.log("---------");

clienteObjeto = {
    nome: "Vinicius",
    idade: "36",
    cpf: "112341342134",
    email: "teste@teste.com",
    telefone : ["1122","3344"],
    dependente:  [{
        nome: "Sarah",
        parentesco: "filho",
        dataNasc: "20/03/2021"
    }]
}
console.log(clienteObjeto);
console.log("---------");
clienteObjeto.dependente.push({
    nome: "Ivete",
    parentesco: "filho mais velho",
    dataNasc: "20/03/1990"
})
console.log(clienteObjeto);
console.log("---------");
clienteObjeto.dependente.forEach(dependente => {
    console.log(dependente);
});
console.log("---------");
const filhoMaisNovo = clienteObjeto.dependente.filter(filho => filho.dataNasc === "20/03/2021");
console.log('Filho Mais novo. O retorno sempre será um array');
console.log(filhoMaisNovo);
console.log(filhoMaisNovo[0]);
console.log(filhoMaisNovo[0].nome);