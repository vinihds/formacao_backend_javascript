// spread operator => copia as propriedades de objetos para outros, espalhando seu conteudo

const fichaGuerreiro = {
    nome: "Aragon",
    classe: "Guerreiro"
}

const equipoGuerreiro = {
    espada: "Anduril",
    capa: "capa elfica +2"
}

//  juntar esses dois objetos em apenas um chamado personagens
{
    const guerreiro = {fichaGuerreiro,equipoGuerreiro}
    console.table(guerreiro);
}
//resultado não é o que eu queria...ainda está espalhado. o nome de cada variavel virou a chave que
//vamos usar os spread operator
console.log('=========================');
{
    const guerreiro = {...fichaGuerreiro,...equipoGuerreiro}
    console.table(guerreiro);
}
console.log('=========================');
//se utilizar o spread operator const variavel = {...variavelUm,...variavelDois} em objetos que tenham chaves e propriedades com o mesmo nome, o javascript vai sobreescrever o valor destas propriedades  a medida que encontra novos valores
const fichaMago = {
    nome: "Gandalf",
    classe: "Mago"
}

const fichaArqueiro = {
    nome: "Legolas",
    classe: "Arqueiro"
}

//os objetos acima tem a mesma propriedade
console.log('=========================');
{
    const personagens = {...fichaGuerreiro,...fichaMago,...fichaArqueiro}
    console.log('personagem');
    console.table(personagens);
}
//observamos que o javascript sobreescreveu as chaves com o mesmo nome a cada ocorrencia, 