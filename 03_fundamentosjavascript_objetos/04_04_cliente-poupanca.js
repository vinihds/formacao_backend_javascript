function Cliente(nome,cpf,email,saldo){
    this.nome = nome;
    this.cpf = cpf;
    this.email = email;
    this.saldo = saldo;
    this.depositar = function(valor){
        this.saldo += valor;
    }
}

function ClientePoupanca(nome,cpf,email,saldo,saldoPoupanca){
    this.saldoPoupanca = saldoPoupanca;
    //preciso relacionar o cliente poupança com o cliente
    //fazer da seguinte maneira, então {Cliente.call} que eu vou chamar ai o construtor do cliente
    // no call o primeiro parametro que recebe é o this desse objeto
    Cliente.call(this,nome,cpf,email,saldo)
}
 const ju = new ClientePoupanca("Vinicius","123123","vinicius@teste.com",0,200);
console.log('============================');
console.log(ju);
console.log('============================');
// Vamos definir agora em um prototipo uma função depositar poupança, será necessario a propriedade prototype e definir a função depositar. Vai receber uma fuction 
ClientePoupanca.prototype.depositarPoupanca = function(valor){
    console.log(`depositou ${valor}`);
    this.saldoPoupanca += valor;
}
ju.depositarPoupanca(300);
console.log('============================');
console.log(ju);
for (registro in ju){
    //console.log(`${registro} - ${ju[registro]}`);
}