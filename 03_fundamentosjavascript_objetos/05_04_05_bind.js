/*
bind prende ou liga uma função ao contexto de um objeto

*/

const personagem = {
    nome: "Princesa Leia",
    apresentar: function(){
        return `a personagem é ${this.nome}`
    }
}

const personagemGenerico = personagem.apresentar
console.log(`teste`);
console.log(personagemGenerico())
/*a personagem é undefined, 
Quando atribuimos apresentar() a viariavel personagemGenerico estamos retirando a funcao apresentar() do contexto
por isso o this nao está mais acessivel 
*/
const personagemDefinido = personagemGenerico.bind(personagem)
console.log(personagemDefinido())
//a personagem é Princesa Leia