//JSON.parse() -> converte JSON para JS
//const objetoJSON = "{\"id\":50}";
const objetoJSON = '{"id":50,"titulo":"Titulo do livro","autor":"Joao","categoria":"programacao","versoes":["ebook","impresso"]}';
console.log('Objeto JSON');
console.log(objetoJSON);
const objetoJS = JSON.parse(objetoJSON);
console.log('Objeto JavaScript');
console.log(objetoJS);