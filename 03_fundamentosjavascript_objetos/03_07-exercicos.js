const paciente = {
    nome: "James T.",
    idade:30,
    email: "jt@email.com",
    identicacao: "Alpha01259859",
    funcao:"comandante",
    peso:80.1,
    altura:1.80,
    calcularIMC:function(){
          return (this.peso/(Math.pow(this.altura,2)))
    },
    nomeCompleto:function(){
      console.log(this.nome)
    }
   }
console.log('=============================');
console.log('exibir as chaves');
for (chave in paciente){
    console.log(chave);
}
console.log('=============================');
console.log('Exibir os valores');
for (valor in paciente){
    console.log(paciente[valor]);
}
console.log('=============================');
console.log('Exibir as chaves sem exibir as funcoes');
for (chave in paciente){
    if (typeof paciente[chave]==="function"){
        continue;
    }else{
        console.log(chave);
    }
}
console.log('=============================');
console.log('Exibir os valores sem exibir as funcoes');
for (valor in paciente){
    if (typeof paciente[valor]==="function"){
        continue;
    }else{
        console.log(paciente[valor]);
    }
}