cliente = {
    nome: "Vinicius",
    idade: "36",
    cpf: "112341342134",
    email: "teste@teste.com",
    telefone : ["1122","3344"],
    dependente:  [{
        nome: "Sarah",
        parentesco: "filho",
        dataNasc: "20/03/2021"
    },{
        nome: "Ivete",
        parentesco: "filho mais velho",
        dataNasc: "20/03/1990"
    }],
    saldo:100,
    depositar:function(valor){
        this.saldo+=valor;
    }
}

console.log(cliente.saldo);
cliente.depositar(300)
console.log(cliente.saldo);