const clienteObjeto = {
    nome: "Vinicius",
    idade: "36",
    cpf: "112341342134"
}

const chaves =  ["nome","idade","cpf"];

console.log(`Acessando o atributo nome primeiro modo ${clienteObjeto.nome}`);
console.log(`Acessando o atributo idade segundo modo ${clienteObjeto["idade"]}`);
console.log(`Acessando o atributo cpf terceiro modo ${clienteObjeto[chaves[2]]}`);
console.log(`Acessando o atributo que não existe, como rg ${clienteObjeto["RG"]} , o resultado será undefined`);

//acessa todos os atributos

chaves.forEach(atributo => {
    console.log(atributo);
    console.log(`Está sendo acessado o atributo ${atributo} com o conteúdo ${clienteObjeto[atributo]}` );
});