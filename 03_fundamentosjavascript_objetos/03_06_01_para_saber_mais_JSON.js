//JSON é um acronimo para JavaScript Object Notation
//é um formato que utiliza a sintaxe de objetos e arrays do javascript
//é extremamente versatil e o metodo mais comum para trabalhar com transferencia de dados entre Cliente/Servidor e é mais facil enteder do q um xml

/*
    O codigo abaixo mostra um JSON com dois conjuntos de propriedade chave/valor
    Primeiro tem um valor de uma String 
    Segundo tem um valor de um array de objetos
    {
        "editora": "Casa do Código",
        "catalogo": [
        {
            "id": 50,
            "titulo": "Primeiros Passos com NodeJS",
            "autor": "João Rubens",
            "categoria": "programação",
            "versoes": ["ebook", "impresso"]
        },
        {
            "id": 59,
            "titulo": "ECMAScript 6",
            "autor": "Diego Martins de Pinho",
            "categoria": "programação",
            "versoes": ["ebook"]
        },
        {
            "id": 39,
            "titulo": "Orientação a Objetos",
            "autor": "Thiago Leite",
            "categoria": "programação",
            "versoes": ["ebook", "impresso"]
        }
    ]}

    Diferenças de sintaxe entre javascript e JSON
        Primeira
            JSON: as chaves deven estar entre aspas duplas ""
            JavaScript: as aspas das chaves não é obrigatório
        Segundo
            JSON: aceita SOMENTE valores primitivos (String, number, boolean, null) array e object
            Java Scriot: Alemd de aceitar valores primitivos (String, number, boolean, null), array, object, funções e metodos
        Terceiro
            Não são permitidas vírgulas após o último conjunto de chave/valor do objeto.


    JSON é um formato criado para transferencia de dados, necessário converter para JavaScript
    JSON.parse() => convert JSON para objeto javascript.json
    JSON.stringify() => convert JS para objeto JSON

    processo de converter estrutura de daos em sequencia de bytes se chama serialização
*/