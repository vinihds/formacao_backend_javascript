cliente = {
    nome: "Vinicius",
    idade: "36",
    cpf: "112341342134",
    email: "teste@teste.com",
    telefone : ["1122","3344"],
    dependente:  [{
        nome: "Sarah",
        parentesco: "filho",
        dataNasc: "20/03/2021"
    },{
        nome: "Ivete",
        parentesco: "filho mais velho",
        dataNasc: "20/03/1990"
    }],
    saldo:100,
    depositar:function(valor){
        this.saldo+=valor;
    }
}

let relatorio = "";
for (let info in cliente){
    //console.log(info);//to pegando a chave
    // console.log(`${info} - ${cliente[info]}`);
    if (typeof cliente[info]==="object" || typeof cliente[info]==="function"){
        continue
    }else{
        relatorio+=`${info}: ${cliente[info]}
         `;
    }

}
console.log(relatorio);